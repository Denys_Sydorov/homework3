import React, { Component } from 'react'

export default class Error extends Component {
    state = { 
        hasError: false
    }
    componentDidCatch(error, info) {
        console.log('error: ', error);
        console.log('info', info);
        this.setState({hasError: true});

    }

    render() {
        if (this.state.hasError) {
            return (
                <>
                    <p>Чет пошло не так</p>
                    <button onClick={()=>console.log('Сообщение об ошибке')}>Сообщить об ошибке</button>
                </>
        )} else {
            return this.props.children;
        }
    }
}
