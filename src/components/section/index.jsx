import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Error from '../error';

import { data } from '../reques';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';

import './section.css'

export default class Section extends Component {

    state = { allCurrency: null }
    componentDidMount() {
        data(this.props.url)
            .then((data1) => {
                this.setState(({ allCurrency }) => {
                    return {
                        allCurrency: data1
                    } // повертає новий об'єкт
                })
            });
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(prevProps.status !== this.props.status){
            console.log("update");
            console.log('prevProps', prevProps);
            console.log('prevState', prevState);
            this.setState({status: this.props.status})
        } else {
            console.log("no update");
            console.log('oldPrevProps', prevProps);
            console.log('oldPrevState', prevState);
        }
    }

    render() {
        const {allCurrency} = this.state;
        return (
            <>
            <Error>                
                    <table>
                        <tbody>
                            <tr>
                                <th>Currency name</th>
                                <th>Price</th>
                                <th>Cod</th>
                            </tr>
                            { Array.isArray(allCurrency) ? 
                            allCurrency.map((item, index) => {
                                return (
                                    <tr key={index + "t"}>
                                        <td>{item.txt}</td>
                                        <td>{item.rate ? item.rate.toFixed(2) : item.value}</td>
                                        <td>{item.cc}</td>
                                        <td><i /* onClick={()=>{onEdit(index)}} */ ><EditIcon /></i></td>
                                        <td><i /* onClick={()=>{onSave(index)}} */ ><SaveIcon /></i></td>
                                    </tr>
                                )
                            }) :  <CircularProgress />}
                        </tbody>
                    </table>                
            </Error>
            </>
        )
    }
}
