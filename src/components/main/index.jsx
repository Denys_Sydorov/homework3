import React, { Component } from 'react';
import Button from '../button';
import Section from '../section';
import Error from '../error';


import {
    BrowserRouter as Router,
    /* Switch, */
    Route,
    /* Link */
} from "react-router-dom";

import './main.css';


class Main extends Component {
    render() {
        // перший спосіб створення запиту
        /*let promise = fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
        promise.then((data) => {return data.json()})
        .then((data1)=> {console.log(data1)})
        */
        return (
            <>
                <Error>
                    <Router>
                        <header>
                            <a href='/currency'><Button value='Офіційний курс гривні до іноземних валют та облікова ціна банківських металів'></Button></a>
                            <a href='/'><Button value='Home'></Button></a>
                            <a href='/bank-info'><Button value='Основні показники діяльності банків України'></Button></a>                            
                        </header>
                        <Route path='/currency' >
                                <Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'>Офіційний курс гривні до іноземних валют та облікова ціна банківських металів</Section>
                            </Route>
                        <Route exact path='/home' >
                            <Section url='http://localhost:3000/' >HOME</Section>
                        </Route>
                        <Route path='/bank-info' >
                            <Section url="https://bank.gov.ua/NBUStatService/v1/statdirectory/basindbank?date=20160101&period=m&json">Основні показники діяльності банків України</Section>
                        </Route>                    
                    </Router>
                </Error>  
            </>     
        )        
    }
}
export default Main;