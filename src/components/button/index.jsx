import React from 'react'

export default function Button({value}) {
    return (
        <>
            <input type='button' value={value}></input>
        </>
    )
}